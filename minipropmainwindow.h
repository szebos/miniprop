#ifndef MINIPROPMAINWINDOW_H
#define MINIPROPMAINWINDOW_H

#include <QMainWindow>
#include <QHash>

#include "profile.h"
#include "Geom_Surface.hxx"
#include "TopoDS_Solid.hxx"
#include "Law_Interpol.hxx"
#include "TopoDS_Wire.hxx"
namespace Ui {
class miniPropMainWindow;
}

class miniPropMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit miniPropMainWindow(QWidget *parent = 0);
    ~miniPropMainWindow();
    void readProfile();
    void buildBlade(const QVector<double> &alphas, const QVector<double> &radii, const QVector<double> &chords);
    void buildKarman();
    void buildHubAndShroud(double r1,double r2);
    void buildBladeSolid();
    void cutBladeSolidTop();
    void buildRotorSolid();
    void addRepetitionsToDoc(const Handle_Geom_Surface &psSurface,const Handle_Geom_Surface &ssSurface,int Np);
    void saveFiles();
    void buildAll();
private:
    Ui::miniPropMainWindow *ui;
    QString currentFile;
    QHash<QString, airfoil*> airFoilsTable;
    QVector<double> radii;
    QVector<double> alphas;
    QVector<double> chords;
    QVector<gp_Pnt2d> dxLawPt;
    QVector<gp_Pnt2d> dyLawPt;
    Law_Interpol dxLaw;
    Law_Interpol dyLaw;
    Law_Interpol cLaw;
    Law_Interpol aLaw;
    void buildBlade(double alpha, double rad, double chord,Handle_Geom_Curve &curve);
    void buildBlade(double alpha, double rad, double chord,Handle_Geom_Curve &curve,double teFillet);
    TopoDS_Wire buildBlade(double alpha, double rad, double chord,Handle_Geom_Curve &pscurve,Handle_Geom_Curve &sscurve,double teFillet);
    void addRepetitionsToDoc(const Handle_Geom_Surface &psSurface,const Handle_Geom_Surface &ssSurface,double alpha);
    void buildKarmanCurve();
    void buildKarmanLETECurves();
    void buildKarmanLETESurf();
    void buildKarmanClosureSurf();
    void builTEFillet();
    void setStackingLaw();
    Handle_Geom_Surface psSurface;
    Handle_Geom_Surface ssSurface;
    Handle_Geom_Surface hubSurface;
    Handle_Geom_Surface shroudSurface;
    Handle_Geom_Surface kLETEpsSurf;
    Handle_Geom_Surface kLETEssSurf;
    Handle_Geom_Surface kpsSurf;
    Handle_Geom_Surface kssSurf;
    Handle_Geom_Curve leCurve;
    Handle_Geom_Curve teCurve;
    Handle_Geom_Curve kCurve;
    Handle_Geom_Curve lepskCurve;
    Handle_Geom_Curve tepskCurve;
    Handle_Geom_Curve lesskCurve;
    Handle_Geom_Curve tesskCurve;
    Handle_Geom_Curve kClosing1psCurve;
    Handle_Geom_Curve kClosing2psCurve;
    Handle_Geom_Curve kClosing1ssCurve;
    Handle_Geom_Curve kClosing2ssCurve;
    TopoDS_Solid bladeBase;
    TopoDS_Solid rotor;
    TopoDS_Face hubF;
    TopoDS_Face shroudF;
    TopoDS_Face psFace;
    TopoDS_Face ssFace;
    TopoDS_Face hubface ;
    TopoDS_Face shroudface;
    int np;
    double myTol;
    double myScale;
    double radTip;
    double radRoot;
    double rHub;
    double rShroud;
    double lChannel;
    double hRotor;
    double eRotor;
    double rFillet;
    double rTEFillet;
    double r1Ell;
    double r2Ell;
    double posLePs,posTePs;
    double posLeSs,posTeSs;
    bool hasKarman;
    bool hasFillet;
    bool hasTEFillet;
    bool hasEllipticRoot;
    bool hasChordLaw;
    bool hasAngleLaw;
    bool fixBladeBalance;
    int variationMode;
    QString dir;
    QString airfoilName;
};

#endif // MINIPROPMAINWINDOW_H
