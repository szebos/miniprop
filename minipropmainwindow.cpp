#include "minipropmainwindow.h"
#include "ui_minipropmainwindow.h"

#include "igesreader.h"
#include "profileGLView.h"

#include "QFileInfo"

#include "TopoDS.hxx"
#include "GeomFill_NSections.hxx"
#include "Geom2d_Line.hxx"
#include "Geom2d_TrimmedCurve.hxx"
#include "Geom2dAdaptor_Curve.hxx"
#include "Geom_Line.hxx"
#include "Geom_Ellipse.hxx"
#include "Geom_TrimmedCurve.hxx"
#include "Geom_SurfaceOfRevolution.hxx"
#include "Geom_RectangularTrimmedSurface.hxx"
#include "GeomConvert_CompCurveToBSplineCurve.hxx"
#include "TColGeom_SequenceOfCurve.hxx"
#include "TColStd_SequenceOfReal.hxx"
#include <TopTools_ListOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include "BRepOffsetAPI_ThruSections.hxx"
#include "BRepBuilderAPI_MakeEdge.hxx"
#include "BRepBuilderAPI_MakeWire.hxx"
#include "BRepBuilderAPI_MakeSolid.hxx"
#include "BRepBuilderAPI_MakeVertex.hxx"
#include <BRepFilletAPI_MakeFillet.hxx>
#include <BRepFilletAPI_MakeFillet2d.hxx>
#include <FilletSurf_Builder.hxx>
#include "BRepBuilderAPI_Sewing.hxx"
#include "BRepFill_PipeShell.hxx"
#include <BRepPrimAPI_MakeCylinder.hxx>
#include <BRepAlgoAPI_Cut.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <TopExp_Explorer.hxx>
#include <IGESControl_Controller.hxx>
#include <IGESControl_Writer.hxx>
#include <ShapeFix_Solid.hxx>
#include <facetoolbox.h>
#include <GProp_GProps.hxx>
#include <BRepGProp.hxx>>
#include <Law_Interpol.hxx>
#include <Geom2dGcc_Circ2dTanOnRad.hxx>
#include <Geom2dGcc_QualifiedCurve.hxx>
miniPropMainWindow::miniPropMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::miniPropMainWindow)
{
    ui->setupUi(this);
    ui->centralWidget->setDocument(new document);
    airfoilName = "E374";
#ifdef __APPLE__
//    currentFile = "/Users/sebastien/workspace/miniProp/profiles/bw3.IGS";
    currentFile = "/Users/sebastien/workspace/miniProp/profiles/"+airfoilName+".IGS";
    dir = "/Users/sebastien/workspace/miniProp/blades/";
#else
//    currentFile = "../miniPropbis/profiles/bw3.IGS";
    currentFile = "../miniPropbis/profiles/"+airfoilName+".IGS";
    dir = "C:/Users/sebastien/Documents/Projets/Mykonos/MK3X049/";
#endif
    myTol = 1e-3;
    myScale=1e-3;
    np=3;
    rHub=12.5;
    rShroud=65;
    lChannel=120;
    rFillet=0.5;
    rTEFillet=0.1;
    hRotor=12.7;
    eRotor=0.9;
    hasKarman=false;
    hasFillet=false;
    hasTEFillet=false;
    hasEllipticRoot=true;
    hasChordLaw=true;
    fixBladeBalance=true;
    r1Ell = 0.35;
    r2Ell = 0.35;
    variationMode=3;

    readProfile();

    radTip =65.1;
    radRoot =15;

    radii<<rHub/3;
    for(int i =9 ; i >=0 ;i--){
        double ksi=i/9.;
        radii<<radTip*(1-ksi)+radRoot*ksi;
    }

    double alphaTip = 9;
    double chordTip = 10;
    for(int i = 1 ; i < radii.size() ;i++){
        double ratio = radTip/radii[i];
        if(variationMode==1){
            ratio = Min(ratio,2.5);
        }
        else if(variationMode==2){
        if(ratio>2)
            ratio = 2*Exp(1-2/ratio);
        }
        else if(variationMode==3){
            ratio = Exp(1-1/ratio);
        }
        alphas<<alphaTip*ratio;
        chords<<chordTip*ratio;
    }
    alphas.insert(0,alphas.first());
    chords.insert(0,10);

//    dxLawPt<<gp_Pnt2d(0.,0.)<<gp_Pnt2d(0.15,0.)<<gp_Pnt2d(0.25,0.)<<gp_Pnt2d(0.5,0.)<<gp_Pnt2d(1.,5.5);
//    dxLawPt<<gp_Pnt2d(0.,-0.5)<<gp_Pnt2d(0.5,-0.75)<<gp_Pnt2d(1.,-0.9);
    dxLawPt<<gp_Pnt2d(0.,-0.5)<<gp_Pnt2d(0.5,-0.5)<<gp_Pnt2d(1.,-0.5);
    dyLawPt<<gp_Pnt2d(0.,0.)<<gp_Pnt2d(0.5,0.)<<gp_Pnt2d(1.,0.0);

    setStackingLaw();

    buildAll();


}

miniPropMainWindow::~miniPropMainWindow()
{
    delete ui;
}

void miniPropMainWindow::setStackingLaw(){
    TColgp_Array1OfPnt2d poindsDx(1,dxLawPt.size());
    TColgp_Array1OfPnt2d poindsDy(1,dyLawPt.size());

    for(int i = 0 ; i < dxLawPt.size() ;i++) poindsDx.SetValue(i+1,dxLawPt[i]);
    for(int i = 0 ; i < dyLawPt.size() ;i++) poindsDy.SetValue(i+1,dyLawPt[i]);

    dxLaw.Set(poindsDx);
    dyLaw.Set(poindsDy);
}


void miniPropMainWindow::buildAll(){
    buildBlade(alphas,radii,chords);
    if(hasKarman) buildKarman();
    addRepetitionsToDoc(psSurface,ssSurface,np);
    buildHubAndShroud(rHub,rShroud);
//    buildBladeSolid();
//    if(hasTEFillet) builTEFillet();
//    if(!hasKarman) cutBladeSolidTop();
//    buildRotorSolid();
//    saveFiles();

}

void miniPropMainWindow::readProfile(){
    igesReader aReader;
    aReader.readIGESFile(currentFile);
    QFileInfo infos(currentFile);
    try{
        Handle_Geom_Curve psCurve = curvesToolBox::curveFromEdge(TopoDS::Edge(aReader.hshapes.Value(1)));
        Handle_Geom_Curve ssCurve = curvesToolBox::curveFromEdge(TopoDS::Edge(aReader.hshapes.Value(2)));

        airfoil *af = new airfoil(psCurve,ssCurve);
        airFoilsTable.insert(infos.fileName().remove(infos.suffix()).remove("."),af);

        ui->profileView->scene()->deleteLater();
        ui->profileView->setScene(new airfoilGLView(af));
    }
    catch(Standard_Failure){
        qDebug("Error processing profile cad.");
    }
}

void miniPropMainWindow::buildBlade(double alpha, double rad, double chord,Handle_Geom_Curve &curve){
    gp_Pnt pivot;
    double pos = (rad-radRoot)/radTip;
    pos=Max(0.,pos);
    double dx = dxLaw.Value(pos);
    double dy = dyLaw.Value(pos);
//    qDebug()<<"dx"<<dx<<dy<<"dy";
    curve->Translate(gp_Vec(dx,dy,0));
    curve->Scale(pivot,chord);
    curve->Rotate(gp_Ax1(pivot,gp_Dir(0,0,1)),-alpha / 180 * M_PI);
    curve->Translate(gp_Vec(0,0,rad));
}

void miniPropMainWindow::buildBlade(double alpha, double rad, double chord, Handle_Geom_Curve &curve, double teFillet){
    gp_Pnt pivot;
    double pos = (rad-radRoot)/radTip;
    pos=Max(0.,pos);
    double dx = dxLaw.Value(pos);
    double dy = dyLaw.Value(pos);

    Handle_Geom2d_Curve curve2d = GeomAPI::To2d(curve,gp_Pln(gp_Pnt(0,0,0),gp_Dir(0,0,1)));
    Geom2dAdaptor_Curve adCurve(curve2d);
//    for(int i = 0 ; i < 100 ;i++ ) qDebug() <<adCurve.Value(adCurve.FirstParameter() * (99.-i)/99. + adCurve.LastParameter() * i / 99.).Y();
    Geom2dGcc_QualifiedCurve qCurve2d(adCurve,GccEnt_unqualified);
    Handle_Geom2d_Line line = new Geom2d_Line(gp_Pnt2d(0,0),gp_Dir2d(1,0));
    Handle_Geom2d_Curve seg = new Geom2d_TrimmedCurve(line,0.5,1.5);
    Geom2dAdaptor_Curve AdLine(seg);
    double localR_TE = teFillet/chord;
    Geom2dGcc_Circ2dTanOnRad myGeom2dGcc_Circ2dTanOnRad( qCurve2d,AdLine,localR_TE,myTol*100);//Scale radius
    int nSol = myGeom2dGcc_Circ2dTanOnRad.NbSolutions();
    if(nSol >= 1){
        gp_Circ2d filletCircle=myGeom2dGcc_Circ2dTanOnRad.ThisSolution(1);
        double parSol,ParArg;
        gp_Pnt2d ptSol;
        myGeom2dGcc_Circ2dTanOnRad.Tangency1(1,parSol,ParArg,ptSol);
        qDebug()<<"center :"<<filletCircle.Location().X()<<filletCircle.Location().Y()<<"angle :"<<parSol;
//        Handle_Geom2d_Curve

    }

    curve->Translate(gp_Vec(dx,dy,0));
    curve->Scale(pivot,chord);
    curve->Rotate(gp_Ax1(pivot,gp_Dir(0,0,1)),-alpha / 180 * M_PI);
    curve->Translate(gp_Vec(0,0,rad));
}

TopoDS_Wire miniPropMainWindow::buildBlade(double alpha, double rad, double chord, Handle_Geom_Curve &pscurve, Handle_Geom_Curve &sscurve, double teFillet){
    gp_Pnt pivot;
    double pos = (rad-radRoot)/radTip;
    pos=Max(0.,pos);
    double dx = dxLaw.Value(pos);
    double dy = dyLaw.Value(pos);
//    qDebug()<<"dx"<<dx<<dy<<"dy";
    pscurve->Translate(gp_Vec(dx,dy,0));
    pscurve->Scale(pivot,chord);
    pscurve->Rotate(gp_Ax1(pivot,gp_Dir(0,0,1)),-alpha / 180 * M_PI);
    pscurve->Translate(gp_Vec(0,0,rad));
    sscurve->Translate(gp_Vec(dx,dy,0));
    sscurve->Scale(pivot,chord);
    sscurve->Rotate(gp_Ax1(pivot,gp_Dir(0,0,1)),-alpha / 180 * M_PI);
    sscurve->Translate(gp_Vec(0,0,rad));

    TopoDS_Wire profile = BRepBuilderAPI_MakeWire(BRepBuilderAPI_MakeEdge(pscurve),
                                                  BRepBuilderAPI_MakeEdge(sscurve));
    TopoDS_Face profileFace = BRepBuilderAPI_MakeFace(profile);
    ui->centralWidget->getDoc()->addShape(profileFace,myScale);
    BRepFilletAPI_MakeFillet2d fillet2d(profileFace);
    TopExp_Explorer explorer(profileFace,TopAbs_VERTEX);
    TopoDS_Vertex Vtx;
//    while (explorer.More()) {
        explorer.Next();
        explorer.Next();
        Vtx = TopoDS::Vertex(explorer.Current());
//    }
        fillet2d.AddFillet(Vtx,rTEFillet);
        fillet2d.Build();
        if(fillet2d.IsDone()){
        explorer.Init(fillet2d.Shape(),TopAbs_WIRE);
      profile = TopoDS::Wire(explorer.Current());
        }
        return profile;
}

void miniPropMainWindow::buildBlade(const QVector<double> & alphas, const QVector<double> &  radii, const QVector<double> &  chords){
    airfoil *af = airFoilsTable.find(airfoilName).value();
    TColGeom_SequenceOfCurve NCps,NCss;
    TColStd_SequenceOfReal W;
    ui->centralWidget->makeCurrent();
    Handle_Geom_Curve psCurve;
    Handle_Geom_Curve ssCurve;
    for(int i = 0 ; i < alphas.size() ;i++){
        if(i==0 && hasEllipticRoot){
            Handle_Geom_Ellipse ell10 = new Geom_Ellipse(gp_Ax2(gp_Pnt(0.5,0,0),gp_Dir(0,0,1)),r1Ell,r2Ell);
            psCurve = new Geom_TrimmedCurve(ell10,0,M_PI);
            ssCurve = new Geom_TrimmedCurve(ell10,M_PI,2*M_PI);
        }
        else {
            psCurve = Handle_Geom_Curve::DownCast(af->profileCurvePS->Copy());
            ssCurve = Handle_Geom_Curve::DownCast(af->profileCurveSS->Copy());
        }
//        buildBlade(alphas[i],radii[i],chords[i],psCurve,rTEFillet);
        ui->centralWidget->getDoc()->addShape(buildBlade(alphas[i],radii[i],chords[i],psCurve,ssCurve,rTEFillet),myScale);
        NCps.Append(psCurve);
        NCss.Append(ssCurve);
        W.Append(i/(alphas.size() -1.));
    }
    GeomFill_NSections psSurfBuilder(NCps,W) ;
    GeomFill_NSections ssSurfBuilder(NCss,W) ;
    psSurface = psSurfBuilder.BSplineSurface();
    ssSurface = ssSurfBuilder.BSplineSurface();

    gp_Vec dx;
    if(fixBladeBalance){
        TopoDS_Shell aShell;
        BRep_Builder aBuilder;
        aBuilder.MakeShell(aShell);
        aBuilder.Add(aShell,BRepBuilderAPI_MakeFace(psSurface,myTol));
        aBuilder.Add(aShell,BRepBuilderAPI_MakeFace(ssSurface,myTol));
        GProp_GProps props;
        BRepGProp::VolumeProperties(aShell,props);
        dx= gp_Vec(-props.CentreOfMass().X(),-props.CentreOfMass().Y(),0);
        psSurface->Translate(dx);
        ssSurface->Translate(dx);
    }

    for(int i = 1 ; i <= NCps.Length() ; i++){
        NCps.Value(i)->Translate(dx);
        NCss.Value(i)->Translate(dx);
        ui->centralWidget->getDoc()->addCurve(NCps.Value(i));
        ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
        ui->centralWidget->getDoc()->addCurve(NCss.Value(i));
        ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
    }

    double u1,u2,v1,v2;
    psSurface->Bounds(u1,u2,v1,v2);
    leCurve = psSurface->UIso(u2);
    teCurve = psSurface->UIso(u1);
    ui->centralWidget->getDoc()->addCurve(teCurve,Qt::blue);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
    ui->centralWidget->getDoc()->addCurve(leCurve,Qt::red);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
}

void miniPropMainWindow::buildKarman(){
    buildKarmanCurve();
    buildKarmanLETECurves();
    buildKarmanLETESurf();
    buildKarmanClosureSurf();
}

void miniPropMainWindow::buildKarmanCurve(){
    gp_Pnt ptLeK,ptTeK;
    gp_Vec TLeK,TTeK;
    double u2Te,u2Le;
    u2Te = leCurve->LastParameter();
    u2Le = teCurve->LastParameter();
    leCurve->D1(u2Le,ptLeK,TLeK);
    teCurve->D1(u2Te,ptTeK,TTeK);
    TLeK.Normalize();
    TTeK.Normalize();

    gp_Pnt pt1K = ptLeK.Translated(TLeK*1);
    gp_Pnt pt4K = ptTeK.Translated(TTeK*5);
    gp_Pnt pt3K = pt4K.Translated(gp_Vec(1,5,0));
    gp_Pnt pt2K = pt3K.Translated(gp_Vec(-2,0,0));

    TColgp_Array1OfPnt poles(1,6);
    poles.SetValue(1,ptLeK);
    poles.SetValue(2,pt1K);
    poles.SetValue(3,pt2K);
    poles.SetValue(4,pt3K);
    poles.SetValue(5,pt4K);
    poles.SetValue(6,ptTeK);
    kCurve = new Geom_BezierCurve(poles);

    TopoDS_Compound aCompound;
    BRep_Builder aBuilder;
    aBuilder.MakeCompound(aCompound);
    aBuilder.Add(aCompound,BRepBuilderAPI_MakeEdge(kCurve));
    for(int i=1;i<=poles.Length();i++) aBuilder.Add(aCompound,BRepBuilderAPI_MakeVertex(poles.Value(i)));

    ui->centralWidget->getDoc()->addShape(aCompound,Qt::black,Qt::magenta,Qt::darkMagenta);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);

    aBuilder.MakeCompound(aCompound);
    for(int i=1;i<=poles.Length()-1;i++)
        aBuilder.Add(aCompound,BRepBuilderAPI_MakeEdge(poles.Value(i),poles.Value(i+1)));
    ui->centralWidget->getDoc()->addShape(aCompound);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
    ui->centralWidget->getDoc()->getLastDShape()->forConstruction=true;

}

void miniPropMainWindow::buildKarmanLETECurves(){
    double u1,u2,v1,v2;
    psSurface->Bounds(u1,u2,v1,v2);
    Handle_Geom_Curve psCurveShroud = psSurface->VIso(v2);
    ssSurface->Bounds(u1,u2,v1,v2);
    Handle_Geom_Curve ssCurveShroud = ssSurface->VIso(v2);

    double u1ps,u2ps,u1ss,u2ss,uleps,uteps,uless,utess;
    u1ps = psCurveShroud->FirstParameter();
    u2ps = psCurveShroud->LastParameter();
    u1ss = ssCurveShroud->FirstParameter();
    u2ss = ssCurveShroud->LastParameter();
    curvesToolBox::relAbsCurveToParameter(psCurveShroud,0.9,uleps,myTol);
    curvesToolBox::relAbsCurveToParameter(psCurveShroud,0.1,uteps,myTol);
    curvesToolBox::relAbsCurveToParameter(ssCurveShroud,0.9,uless,myTol);
    curvesToolBox::relAbsCurveToParameter(ssCurveShroud,0.1,utess,myTol);

    kClosing2psCurve  = new Geom_TrimmedCurve(psCurveShroud,uteps,uleps);
    kClosing2ssCurve  = new Geom_TrimmedCurve(ssCurveShroud,utess,uless);

    lepskCurve = new Geom_TrimmedCurve(psCurveShroud,uleps,u2ps);
    ui->centralWidget->getDoc()->addCurve(lepskCurve);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);

    tepskCurve = new Geom_TrimmedCurve(psCurveShroud,u1ps,uteps);
    ui->centralWidget->getDoc()->addCurve(tepskCurve);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);

    lesskCurve = new Geom_TrimmedCurve(ssCurveShroud,uless,u2ss);
    ui->centralWidget->getDoc()->addCurve(lesskCurve);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);

    tesskCurve = new Geom_TrimmedCurve(ssCurveShroud,u1ss,utess);
    ui->centralWidget->getDoc()->addCurve(tesskCurve);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
}

void miniPropMainWindow::buildKarmanLETESurf(){
    TopoDS_Wire aWire = BRepBuilderAPI_MakeWire(BRepBuilderAPI_MakeEdge(kCurve));
    TopExp_Explorer explorer;
    double u1,u2,v1,v2;

    BRepFill_PipeShell pipeOpPs(aWire);
    pipeOpPs.Add(BRepBuilderAPI_MakeWire(BRepBuilderAPI_MakeEdge(lepskCurve)));
    pipeOpPs.Add(BRepBuilderAPI_MakeWire(BRepBuilderAPI_MakeEdge(tepskCurve)));
    pipeOpPs.Build();
    ui->centralWidget->getDoc()->addShape(pipeOpPs.Shape());
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
    explorer.Init(pipeOpPs.Shape(),TopAbs_FACE);
    kLETEpsSurf = BRep_Tool::Surface(TopoDS::Face(explorer.Current()));
    kLETEpsSurf->Bounds(u1,u2,v1,v2);
    kClosing1psCurve = kLETEpsSurf->UIso(u1);
//    ui->centralWidget->getDoc()->addCurve(kClosing1psCurve,Qt::yellow);
//    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);

    BRepFill_PipeShell pipeOpSs(aWire);
    pipeOpSs.Add(BRepBuilderAPI_MakeWire(BRepBuilderAPI_MakeEdge(lesskCurve)));
    pipeOpSs.Add(BRepBuilderAPI_MakeWire(BRepBuilderAPI_MakeEdge(tesskCurve)));
    pipeOpSs.Build();
    ui->centralWidget->getDoc()->addShape(pipeOpSs.Shape());
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
    explorer.Init(pipeOpSs.Shape(),TopAbs_FACE);
    kLETEssSurf = BRep_Tool::Surface(TopoDS::Face(explorer.Current()));
    kLETEssSurf->Bounds(u1,u2,v1,v2);
    kClosing1ssCurve = kLETEssSurf->UIso(u2);
//    ui->centralWidget->getDoc()->addCurve(kClosing1ssCurve,Qt::yellow);
//    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
}

void miniPropMainWindow::buildKarmanClosureSurf(){
    TColGeom_SequenceOfCurve NCps,NCss;
    TColStd_SequenceOfReal W;
    kClosing1psCurve->Reverse();

    NCps.Append(kClosing1psCurve);
    NCps.Append(kClosing2psCurve);
    NCss.Append(kClosing1ssCurve);
    NCss.Append(kClosing2ssCurve);
    W.Append(0);
    W.Append(1);
    GeomFill_NSections psSurfBuilder(NCps,W) ;
    GeomFill_NSections ssSurfBuilder(NCss,W) ;

    qDebug()<<"N u poles :"<<psSurfBuilder.BSplineSurface()->NbUPoles()<<ssSurfBuilder.BSplineSurface()->NbUPoles();
    qDebug()<<"N v poles :"<<psSurfBuilder.BSplineSurface()->NbVPoles()<<ssSurfBuilder.BSplineSurface()->NbVPoles();
    TopoDS_Compound aCompound;
    BRep_Builder aBuilder;
    aBuilder.MakeCompound(aCompound);
    for(int iu = 1 ; iu <= psSurfBuilder.BSplineSurface()->NbUPoles();iu++){
        for(int iv = 1 ; iv <= psSurfBuilder.BSplineSurface()->NbVPoles();iv++){
            aBuilder.Add(aCompound,BRepBuilderAPI_MakeVertex(ssSurfBuilder.BSplineSurface()->Pole(iu,iv)));
        }
    }

    ui->centralWidget->getDoc()->addShape(aCompound);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);

    kpsSurf = psSurfBuilder.BSplineSurface();
    kssSurf = ssSurfBuilder.BSplineSurface();

    ui->centralWidget->getDoc()->addShape(BRepBuilderAPI_MakeFace(kpsSurf,myTol));
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
    ui->centralWidget->getDoc()->addShape(BRepBuilderAPI_MakeFace(kssSurf,myTol));
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
}

void miniPropMainWindow::buildBladeSolid(){
    double u1,u2,v1,v2;
    psSurface->Bounds(u1,u2,v1,v2);
    Handle_Geom_Curve psCurveHub = psSurface->VIso(v1);
    Handle_Geom_Curve psCurveShroud = psSurface->VIso(v2);
    ssSurface->Bounds(u1,u2,v1,v2);
    Handle_Geom_Curve ssCurveHub = ssSurface->VIso(v1);
    Handle_Geom_Curve ssCurveShroud = ssSurface->VIso(v2);

    BRep_Builder aBuilder;
    TopoDS_Wire hubWire,shroudWire;
    aBuilder.MakeWire(hubWire);
    aBuilder.MakeWire(shroudWire);
    QList<TopoDS_Edge> anEdgeList;

    anEdgeList << BRepBuilderAPI_MakeEdge(psCurveHub);
    anEdgeList << BRepBuilderAPI_MakeEdge(ssCurveHub);
    hubWire=faceToolBox::makeWire(anEdgeList,myTol);

    anEdgeList.clear();

    anEdgeList << BRepBuilderAPI_MakeEdge(psCurveShroud);
    anEdgeList << BRepBuilderAPI_MakeEdge(ssCurveShroud);
    shroudWire=faceToolBox::makeWire(anEdgeList,myTol);

    hubface = BRepBuilderAPI_MakeFace(hubWire);
    shroudface = BRepBuilderAPI_MakeFace(shroudWire);
    psFace = BRepBuilderAPI_MakeFace(psSurface,myTol);
    ssFace = BRepBuilderAPI_MakeFace(ssSurface,myTol);

    BRepBuilderAPI_Sewing sewOp(myTol);
    sewOp.Add(hubface);
    sewOp.Add(psFace);
    sewOp.Add(ssFace);
    sewOp.Add(shroudface);
    if(hasKarman){
        sewOp.Add(BRepBuilderAPI_MakeFace(kpsSurf,myTol));
        sewOp.Add(BRepBuilderAPI_MakeFace(kssSurf,myTol));
        sewOp.Add(BRepBuilderAPI_MakeFace(kLETEpsSurf,myTol));
        sewOp.Add(BRepBuilderAPI_MakeFace(kLETEssSurf,myTol));
    }
    sewOp.Perform();

//    bladeBase = BRepBuilderAPI_MakeSolid(TopoDS::Shell(sewOp.SewedShape()));
    ShapeFix_Solid sfs;
    bladeBase = sfs.SolidFromShell(TopoDS::Shell(sewOp.SewedShape()));

    ui->centralWidget->getDoc()->clear();
    ui->centralWidget->getDoc()->addShape(bladeBase);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);

}

void miniPropMainWindow::cutBladeSolidTop(){

    BRepPrimAPI_MakeCylinder cylOp1(gp_Ax2(gp_Pnt(0,-lChannel/2,0),gp_Dir(0,1,0),gp_Dir(1,0,0)),rShroud*2,lChannel);
    BRepPrimAPI_MakeCylinder cylOp2(gp_Ax2(gp_Pnt(0,-2*lChannel/2,0),gp_Dir(0,1,0),gp_Dir(1,0,0)),rShroud,2*lChannel);
    BRepAlgoAPI_Cut cutOp1(cylOp1.Solid(),cylOp2.Solid());
    TopExp_Explorer explorer;
    explorer.Init(cutOp1.Shape(),TopAbs_SOLID);
    TopoDS_Solid cuttingShape= TopoDS::Solid(explorer.Current());

    BRepAlgoAPI_Cut cutOp2(bladeBase,cuttingShape);
    explorer.Init(cutOp2.Shape(),TopAbs_SOLID);
    bladeBase = TopoDS::Solid(explorer.Current());
    ui->centralWidget->getDoc()->getLastDShape()->setShape(bladeBase);

}

void miniPropMainWindow::buildRotorSolid(){

    ui->centralWidget->getDoc()->clear();

    BRepPrimAPI_MakeCylinder cylOp1(gp_Ax2(gp_Pnt(0,-hRotor/2,0),gp_Dir(0,1,0),gp_Dir(1,0,0)),rHub,hRotor);
    gp_Trsf rot;
    rotor = cylOp1.Solid();

    rot.SetRotation(gp_Ax1(gp_Pnt(),gp_Dir(0,1,0)),-M_PI/2./np);
    rotor = TopoDS::Solid(rotor.Moved(rot));

    for(int i = 0 ; i < np ;i++){
        double alpha = i / (double) np * 2. * M_PI;
        rot.SetRotation(gp_Ax1(gp_Pnt(),gp_Dir(0,1,0)),alpha);
        TopoDS_Solid aShape = TopoDS::Solid(bladeBase.Moved(rot));
//        TopoDS_Solid aShape = TopoDS::Solid(bladeBase);
//        ui->centralWidget->getDoc()->addShape(aShape);
//        ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);

        BRepAlgoAPI_Fuse fuseOp(rotor,aShape);
//        ui->centralWidget->getDoc()->addShape(fuseOp.Shape());
//        ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
        qDebug()<<fuseOp.ErrorStatus();
        //It happends that several bodies are created, we need to sort them
        TopExp_Explorer explorer;
        explorer.Init(fuseOp.Shape(),TopAbs_SOLID);
        int solCount=0;
        double vol=0;
        TopoDS_Shape lardgestShape;
        while(explorer.More()){
            solCount++;
            GProp_GProps props;
            BRepGProp::VolumeProperties(explorer.Current(),props);
            if(props.Mass()>=vol) {
                vol = props.Mass();
                lardgestShape = explorer.Current();
            }
            explorer.Next();

        }
        qDebug()<<"Result has :"<<solCount<<"solids";
        rotor = TopoDS::Solid(lardgestShape);
//        ui->centralWidget->getDoc()->addShape(fuseOp.Shape());
//        ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
        if(hasFillet){
            ShapeFix_Solid fixSolid(rotor);
            fixSolid.SetPrecision(myTol);
            fixSolid.SetMaxTolerance(myTol);
            fixSolid.SetMinTolerance(myTol/10.);
            explorer.Init(fixSolid.Shape(),TopAbs_SOLID);
            rotor = TopoDS::Solid(explorer.Current());


            BRepFilletAPI_MakeFillet filletOp(rotor,ChFi3d_Polynomial);
            TopTools_ListIteratorOfListOfShape iterator(fuseOp.SectionEdges());
            while (iterator.More()) {
                filletOp.Add(rFillet,TopoDS::Edge(iterator.Value()));
                iterator.Next();
            }

            int Nc = filletOp.NbContours();
            try{
                filletOp.Build();
                TopExp_Explorer explorer(filletOp.Shape(),TopAbs_SOLID);
                rotor = TopoDS::Solid(explorer.Current());
                qDebug()<<"Fillet "<<i<<"built";
            }
            catch(Standard_Failure){
                for(int i=1;i<=Nc;i++){
                    qDebug()<<"FaultyContour"<<filletOp.FaultyContour(i);
                }
            }
        }
    }

    BRepPrimAPI_MakeCylinder cylOp2(gp_Ax2(gp_Pnt(0,-hRotor,0),gp_Dir(0,1,0),gp_Dir(1,0,0)),rHub-eRotor,hRotor*2);
    BRepAlgoAPI_Cut cutOp(rotor,cylOp2.Solid());
    TopExp_Explorer explorer;
    explorer.Init(cutOp.Shape(),TopAbs_SOLID);
    rotor = TopoDS::Solid(explorer.Current());


    ui->centralWidget->getDoc()->addShape(rotor);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);

}


void miniPropMainWindow::addRepetitionsToDoc(const Handle_Geom_Surface &psSurface, const Handle_Geom_Surface &ssSurface, int Np){
    for(int i =0 ; i <= Np ; i++){
        double alpha = i / (double) Np * 2. * M_PI;
        addRepetitionsToDoc(psSurface,ssSurface,alpha);
    }
}

void miniPropMainWindow::addRepetitionsToDoc(const Handle_Geom_Surface &psSurface, const Handle_Geom_Surface &ssSurface, double alpha){
    gp_Trsf rot;
    rot.SetRotation(gp_Ax1(gp_Pnt(),gp_Dir(0,1,0)),alpha);
    Handle_Geom_Surface psCpy = Handle_Geom_Surface::DownCast(psSurface->Copy());
    Handle_Geom_Surface ssCpy = Handle_Geom_Surface::DownCast(ssSurface->Copy());
    psCpy->Transform(rot);
    ssCpy->Transform(rot);
    TopoDS_Face ps =  BRepBuilderAPI_MakeFace(psCpy,myTol);
    TopoDS_Face ss =  BRepBuilderAPI_MakeFace(ssCpy,myTol);
    ui->centralWidget->getDoc()->addShape(ps);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
//    ui->centralWidget->getDoc()->getLastDShape()->showCurvesOrientation=true;
    ui->centralWidget->getDoc()->addShape(ss);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
//    ui->centralWidget->getDoc()->getLastDShape()->showCurvesOrientation=true;
}

void miniPropMainWindow::buildHubAndShroud(double r1, double r2){
    gp_Pnt p1Hub(0,-lChannel/2.,r1);
    gp_Pnt p2Hub(0,lChannel/2.,r1);
    gp_Pnt p1Shroud(0,-lChannel/2.,r2);
    gp_Pnt p2Shroud(0,lChannel/2.,r2);

    Handle_Geom_Line hub = new Geom_Line(p1Hub,gp_Dir(0,1,0));
    Handle_Geom_Line shroud = new Geom_Line(p1Shroud,gp_Dir(0,1,0));

    Handle_Geom_TrimmedCurve hubT = new Geom_TrimmedCurve(hub,0,lChannel);
    Handle_Geom_TrimmedCurve shroudT = new Geom_TrimmedCurve(shroud,0,lChannel);

    double alpha = M_PI / np;
    gp_Ax1 revAx(gp_Pnt(),gp_Dir(0,1,0));
    hubT->Rotate(revAx,-alpha);
    shroudT->Rotate(revAx,-alpha);

    hubSurface = new Geom_SurfaceOfRevolution(hubT,revAx);
    shroudSurface = new Geom_SurfaceOfRevolution(shroudT,revAx);

    double u1,u2,v1,v2;
    hubSurface->Bounds(u1,u2,v1,v2);
    hubSurface = new Geom_RectangularTrimmedSurface(hubSurface,u1,2*alpha,v1,v2);
    shroudSurface->Bounds(u1,u2,v1,v2);
    shroudSurface = new Geom_RectangularTrimmedSurface(shroudSurface,u1,2*alpha,v1,v2);

    hubF =  BRepBuilderAPI_MakeFace(hubSurface,myTol);
    shroudF =  BRepBuilderAPI_MakeFace(shroudSurface,myTol);
    ui->centralWidget->getDoc()->addShape(hubF);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
    ui->centralWidget->getDoc()->addShape(shroudF);
    ui->centralWidget->getDoc()->getLastDShape()->setScale(myScale);
    ui->centralWidget->getDoc()->getLastDShape()->setTransparent(true);
}


void miniPropMainWindow::builTEFillet(){
    double u1,u2,v1,v2;
    psSurface->Bounds(u1,u2,v1,v2);
    gp_Pnt pTeHub = psSurface->Value(u1,v1);
    gp_Pnt pTeShroud = psSurface->Value(u1,v2);

    TopoDS_Edge TeEdge;
    TopExp_Explorer explorer;
    explorer.Init(bladeBase,TopAbs_EDGE);
    while (explorer.More()) {
        TopoDS_Edge anEdge = TopoDS::Edge(explorer.Current());
        Handle_Geom_Curve aCurve = BRep_Tool::Curve(anEdge,u1,u2);
        if( (aCurve->Value(u1).Distance(pTeHub)<myTol && aCurve->Value(u2).Distance(pTeShroud)<myTol) ||
                (aCurve->Value(u2).Distance(pTeHub)<myTol && aCurve->Value(u1).Distance(pTeShroud)<myTol)){
            TeEdge = anEdge;
            break;
        }
        explorer.Next();
    }

    if(TeEdge.IsNull()) return;

    BRepFilletAPI_MakeFillet filletOp(bladeBase,ChFi3d_Polynomial);
    filletOp.SetParams(0.1,0.1,0.1,0.1,.1,0.1);
    filletOp.Add(rTEFillet,TeEdge);

//    TopTools_ListOfShape edgesList;
//    edgesList.Append(TeEdge);
//    FilletSurf_Builder filletOp(bladeBase,edgesList,rTEFillet);

    try{
        filletOp.Build();
        explorer.Init(filletOp.Shape(),TopAbs_SHELL);
//        filletOp.Perform();

        ShapeFix_Solid sfs;
        bladeBase = sfs.SolidFromShell(TopoDS::Shell(explorer.Current()));

        ui->centralWidget->getDoc()->clear();
        ui->centralWidget->getDoc()->addShape(bladeBase,myScale);
//        ui->centralWidget->getDoc()->addShape(BRepBuilderAPI_MakeFace(filletOp.SurfaceFillet(1),myTol));
    }
    catch(Standard_Failure){
        qDebug("Faill to build fillet");
//        filletOp.Simulate(1);
//        ui->centralWidget->getDoc()->addShape(filletOp.Shape(),myScale);
    }
}

void miniPropMainWindow::saveFiles(){



    IGESControl_Controller::Init();
    IGESControl_Writer writerSurfsAero( "write.iges.unit",0 );

    writerSurfsAero.AddShape(hubF);
    writerSurfsAero.AddShape(shroudF);
    writerSurfsAero.AddShape(psFace);
    writerSurfsAero.AddShape(ssFace);


    writerSurfsAero.ComputeModel();
    writerSurfsAero.Write((dir+ "heliceSurfs.igs").toAscii() );

    IGESControl_Writer writerSurfsMeca( "write.iges.unit",0 );

    writerSurfsMeca.AddShape(rotor);


    writerSurfsMeca.ComputeModel();
    writerSurfsMeca.Write((dir+ "heliceMeca.igs").toAscii() );

    QFile aFile(dir+"geomdata.txt");
    aFile.open(QFile::ReadWrite);
    QTextStream out(&aFile);
    out<<airfoilName<<"\n";
    out<<"alpha : \n";
    for(int i=0;i<alphas.size();i++) out<<alphas[i]<<"\t";
    out<<"\n";
    out<<"rad : \n";
    for(int i=0;i<radii.size();i++) out<<radii[i]<<"\t";
    out<<"\n";
    out<<"chrod :\n";
    for(int i=0;i<chords.size();i++) out<<chords[i]<<"\t";
    out<<"\n";
    out<<"dxLaw :\n";
    for(int i=0;i<dxLawPt.size();i++) out<<dxLawPt[i].X()<<";"<<dxLawPt[i].Y()<<"\t";
    out<<"\n";
    out<<"dyLaw :\n";
    for(int i=0;i<dyLawPt.size();i++) out<<dyLawPt[i].X()<<";"<<dyLawPt[i].Y()<<"\t";
    out<<"\n";
    out<<"Loi de variation :\t"<<variationMode<<"\n";
    if(hasEllipticRoot){
        out<< "elliptic root :\n";
        out<< "R1\t"<<r1Ell<<"\t";
        out<< "R2\t"<<r2Ell<<"\n";
    }
    if(hasTEFillet){
        out<<"te fillet :\n";
        out<<"r te\t"<<rTEFillet<<"\n";
    }
    aFile.close();
}
