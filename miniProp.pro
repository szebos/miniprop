#-------------------------------------------------
#
# Project created by QtCreator 2013-11-22T17:04:49
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
include(../turboGeom/OCCTimports.pri)
CONFIG += qt3d_deploy_pkg
include(../qt3d/pkg.pri)

TARGET = miniProp
DESTDIR = ../bin
TEMPLATE = app


SOURCES += main.cpp\
        minipropmainwindow.cpp

HEADERS  += minipropmainwindow.h

FORMS    += minipropmainwindow.ui

INCLUDEPATH +=../turboGeom/meshTools/inc/
INCLUDEPATH +=../turboGeom/meshTools/
INCLUDEPATH +=../turboGeom/turboGeom/inc/
INCLUDEPATH +=../turboGeom/turboGeomTools/inc/
INCLUDEPATH +=../turboGeom/turboGeomViews/inc/
INCLUDEPATH +=../turboGeom/OCCTTurboGeomExt/inc/
INCLUDEPATH +=../turboGeom/OCCTTurboGeomExt/
INCLUDEPATH +=../turboGeom/topoTools
INCLUDEPATH +=../turboGeom/voluteGeom
INCLUDEPATH +=../turboGeom/voluteMesh

CONFIG(debug,debug|release){
    LIBS+=-L../lib
    LIBS+=-lmeshToolsd
    LIBS+=-lturboGeomd
    LIBS+=-lturboGeomToolsd
    LIBS+=-lturboGeomViewsd
    LIBS+=-lOCCTTurboGeomExtd
    LIBS+=-lsolver1drz
    LIBS+=-ltopoToolsd
    LIBS+=-lvoluteGeomd
    LIBS+=-lvoluteMeshd
}
else{
    LIBS+=-L../lib
    LIBS+=-lmeshTools
    LIBS+=-lturboGeom
    LIBS+=-lturboGeomTools
    LIBS+=-lturboGeomViews
    LIBS+=-lOCCTTurboGeomExt
    LIBS+=-lsolver1drz
    LIBS+=-ltopoTools
    LIBS+=-lvoluteGeom
}
